#!/bin/bash
# set -x

# Folgt dem möglichen System Link bis er den Pfad und Ort des Scripts heraus gefunden hat.
SCRIPT_PATH="${BASH_SOURCE}"
while [ -L "${SCRIPT_PATH}" ]; do
  SCRIPT_DIR="$(cd -P "$(dirname "${SCRIPT_PATH}")" >/dev/null 2>&1 && pwd)"
  SCRIPT_PATH="$(readlink "${SCRIPT_PATH}")"
  [[ ${SCRIPT_PATH} != /* ]] && SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_PATH}"
done
SCRIPT_PATH="$(readlink -f "${SCRIPT_PATH}")"
SCRIPT_DIR="$(cd -P "$(dirname -- "${SCRIPT_PATH}")" >/dev/null 2>&1 && pwd)"

VIRTUAL_ENV_LOCATION=$SCRIPT_DIR'/venv/'
PYTHON_MAIN=$SCRIPT_DIR'/src/kassenwart/main.py'
PYTHON_CALL='python'

export PYTHONPATH="$PYTHONPATH:$SCRIPT_DIR"

if [ -d "$VIRTUAL_ENV_LOCATION" ]; then
  source "$VIRTUAL_ENV_LOCATION"/bin/activate
else
  echo "WARNING: No Virtual environment specified"
fi

$PYTHON_CALL "$PYTHON_MAIN" "$@"
