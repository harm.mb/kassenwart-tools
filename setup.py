import os.path

from setuptools import setup, find_packages

import src

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as fd:
    long_description = fd.read()

setup(
    name='kassenwart_cli',
    version=src.__version__,
    description='Ein kleines CLI Tool, was die Arbeit eines:einer Kassenwart:in erleichtert',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Harm Meyer-Borstel',
    author_email='harm.meyer-borstel@pm.me',
    packages=find_packages(include=['src', 'src.*']),
    include_package_data=True,
    install_requires=open('requirements.txt').read().splitlines(),
    python_requires='>=3',
    entry_points={
        'console_scripts': [
            'kassenwart_cli = src.kassenwart.main:cli'
        ]
    },
)
