import csv
import dataclasses


def write_to_csv(export_file, obj_list) -> int:
    """
    Schreibt eine Liste an dataclasses in eine csv Datei.
    Gibt die Anzahl an geschriebenen Zeilen zurück
    """
    if len(obj_list) == 0:
        return 0
    if not hasattr(obj_list[0], '__dataclass_fields__'):
        return 0

    fieldnames = [field.name for field in dataclasses.fields(obj_list[0].__class__)]
    writer = csv.DictWriter(export_file, fieldnames)
    writer.writeheader()
    count = 0
    for obj in obj_list:
        writer.writerow(obj.__dict__)
        count += 1

    return count


def replace_umlaute(string: str) -> str:
    string = string.replace('ä', 'ae')
    string = string.replace('ö', 'oe')
    string = string.replace('ü', 'ue')
    string = string.replace('ß', 'ss')

    return string
