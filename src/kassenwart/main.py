from io import TextIOWrapper

import click
from tabulate import tabulate

import src
from src.kassenwart import utils
from src.kassenwart.commands.extract.banking import BankingExtraction
from src.kassenwart.commands.extract.participant import ParticipantExtraction
from src.kassenwart.commands.reminder import reminder_group
from src.kassenwart.commands.unpaid import resolve_unpaid

NO_EXPORT_FILE_HINT = "Hinweis: Füge die --export Option hinzu um die Ausgabe in einer Datei zu speichern"
TABLE_STYLE = "rounded_grid"
DEFAULT_FILE_NAME_TEILNEHMENDE = "./teilnehmende.csv"
DEFAULT_FILE_NAME_BUCHUNGEN = "./buchungen.csv"


@click.group()
@click.version_option(version=src.__version__, prog_name='kassenwart_cli')
@click.pass_context
def cli(ctx: click.Context):
    """Ein kleines CLI Tool, was die Arbeit eines:einer Kassenwart:in erleichtert."""
    ctx.help_option_names = ['-h', '--help']


@cli.command(no_args_is_help=False)
@click.option('-b', '--buchungen', 'transactions_file',
              required=True,
              default=DEFAULT_FILE_NAME_BUCHUNGEN,
              type=click.Path(exists=True, readable=True, resolve_path=True),
              help="Name einer CSV oder MT940 Datei mit den Buchungseinträgen")
@click.option('-t', '--teilnehmende', 'participants_file',
              required=True,
              default=DEFAULT_FILE_NAME_TEILNEHMENDE,
              type=click.Path(exists=True, readable=True, resolve_path=True),
              help="Name einer CSV, TXT oder PDF Datei mit den Teilnehmenden")
@click.option('-s', '--suche', 'search_name',
              type=click.STRING,
              help="Sucht nach dem Namen eines Teilnehmenden")
@click.option('-i', '--info', 'info',
              is_flag=True,
              help="Zeigt weitere Informationen an")
def unpaid(transactions_file: str | list[str],
           participants_file: str | list[str],
           search_name: str,
           info: bool):
    """
    Zeigt Teilnehmer:innen mit ausstehenden Zahlungen

    Extrahiert alle Teilnehmenden aus den PDF-Listen und versucht diese
    einem Geldeingang zuzuordnen. Dies wird tabellarisch dargestellt oder in eine CSV Datei exportiert.
    Das Script braucht diese ausfüllbaren PDF-Listen oder ein CSV Extract aus dieser Anwendung, und ein Export aus dem
    Onlinebanking im MT940 Format oder dem GnuCash CSV Format.
    """
    participants = ParticipantExtraction(participants_file).get_participants()
    transactions = BankingExtraction(transactions_file).get_transactions()
    unpaid_result = resolve_unpaid(transactions, participants, search_name)
    participant_count = len(participants)
    if search_name:
        participant_count = len(list(filter(
            lambda x: x.match_name(search_name), participants)))

    if len(unpaid_result.paid):
        click.echo('\nBezahlt:')
        for p, t in unpaid_result.paid:
            click.echo(f'\t* {p.fullname()}')
            info and click.echo(f'\t  - {t}\n')

    if len(unpaid_result.unclear):
        click.echo('\nUnklare Geldeingänge:')
        for p, t_list in unpaid_result.unclear:
            click.echo(f'\t* {p.fullname()}')
            info and [click.echo(f'\t  - {t}') for t in t_list] and click.echo()

    if len(unpaid_result.unpaid):
        click.echo('\nOhne Eindeutigen Geldeingang:')
        for p in unpaid_result.unpaid:
            click.echo(f'\t* {p.fullname()}')

    if len(unpaid_result.matchless_transactions):
        click.echo('\nNicht zuordenbare Geldeingänge:')
        for t in unpaid_result.matchless_transactions:
            click.echo(f'\t* {t}')

    click.echo('\n\t\tZusammenfassung')
    click.echo('\t\t===============\n')
    click.echo(f'Teilnehmende:\t {participant_count}')
    click.echo(f'Bezahlt:\t {len(unpaid_result.paid)}')
    click.echo(f'Unbezahlt:\t {len(unpaid_result.unpaid)}')
    click.echo(f'Unklar:\t\t {len(unpaid_result.unclear)}')
    click.echo(f'Kontoeinträge:\t {len(transactions)}')
    return 0


@cli.group()
def extract():
    """Extrahiert CSV Daten aus exportierten Dateien"""
    pass


@extract.command(no_args_is_help=True)
@click.argument('files',
                metavar='MT940-FILES',
                nargs=-1,
                type=click.Path(exists=True, readable=True, resolve_path=True))
@click.option('-e', '--export', 'export_file',
              type=click.File(mode='w'),
              help="Name einer CSV Datei, in welche die Buchungen exportiert werden")
def buchungen(files: str | list[str],
              export_file: TextIOWrapper):
    """
    Extrahiert alle Teilnehmenden aus MT-940 Dateien

    \b
    MT-940-FILES \tMT-940 Buchungsdateien aus dem Online-Banking
    """
    transaction_list = BankingExtraction(files).get_transactions()

    if export_file:
        exported_count = utils.write_to_csv(export_file, transaction_list)
        click.echo(f"{exported_count} Einträge exportiert!")
    else:
        click.echo(tabulate(
            [p.__dict__ for p in transaction_list],
            headers='keys',
            tablefmt=TABLE_STYLE
        ))
        click.echo(NO_EXPORT_FILE_HINT)

    return 0


@extract.command(no_args_is_help=True)
@click.argument('files',
                metavar='TN-LISTEN',
                nargs=-1,
                type=click.Path(exists=True, readable=True, resolve_path=True))
@click.option('-e', '--export', 'export_file',
              type=click.File(mode="w"),
              help="Name einer CSV Datei, in welche die Teilnehmenden exportiert werden")
def teilnehmende(files: str | list[str],
                 export_file: TextIOWrapper):
    """
    Extrahiert alle Teilnehmenden aus TN-Listen

    \b
    TN-LISTEN \tTeilnehmenden-Listen im PDF, TXT oder CSV Format. (Es können auch mehrere angegeben werden)
    """
    participant_list = ParticipantExtraction(files).get_participants()

    if export_file:
        exported_count = utils.write_to_csv(export_file, participant_list)
        click.echo(f"{exported_count} Einträge exportiert!")
    else:
        click.echo(tabulate(
            [p.__dict__ for p in participant_list],
            headers='keys',
            tablefmt=TABLE_STYLE
        ))
        click.echo(NO_EXPORT_FILE_HINT)

    return 0


cli.add_command(reminder_group, 'reminder')

if __name__ == "__main__":
    cli()
