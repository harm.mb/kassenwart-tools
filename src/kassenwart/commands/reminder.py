import csv
import os
import re
import shutil
from dataclasses import dataclass
from io import TextIOWrapper
from pathlib import Path
from typing import Dict

import click
from click import BadParameter

import src


@dataclass
class UnpaidRow:
    name: str
    aktion_list: Dict[str, str]
    summe: str

    def render_aktion_list(self) -> str:
        out = ""
        for aktion, amount in self.aktion_list.items():
            if out:
                out += "\n"
            out += f"\t- {aktion}:\t\t {amount}"
        return out


class TemplateRenderer:
    TEMPLATE_NAME_PATTERN = re.compile(r"({name})")
    TEMPLATE_LIST_PATTERN = re.compile(r"({list})")
    TEMPLATE_SUMME_PATTERN = re.compile(r"({summe})")

    def __init__(self,
                 template_file: TextIOWrapper,
                 unpaid_table_file: TextIOWrapper):
        self.unpaid: list[UnpaidRow] = []
        self.__load_template(template_file)
        self.__load_unpaid_table(unpaid_table_file)

    def __load_template(self, template_file: TextIOWrapper) -> None:
        if not template_file:
            raise BadParameter("Es muss eine Template Datei angegeben werden")
        if not template_file.name.lower().endswith('.txt'):
            raise BadParameter("Die Template Datei muss im .txt Format vorliegen")

        template_str = template_file.read()
        if not self.TEMPLATE_NAME_PATTERN.search(template_str):
            raise BadParameter("{name} wurde nicht im Template gefunden")
        if not self.TEMPLATE_LIST_PATTERN.search(template_str):
            raise BadParameter("{list} wurde nicht im Template gefunden")
        if not self.TEMPLATE_SUMME_PATTERN.search(template_str):
            raise BadParameter("{summe} wurde nicht im Template gefunden")

        self.template = template_str

    def __load_unpaid_table(self, unpaid_table_file: TextIOWrapper) -> None:
        if not unpaid_table_file:
            raise BadParameter("Es muss eine CSV Tabelle mit Mahnungen angegeben werden")
        if not unpaid_table_file.name.lower().endswith('.csv'):
            raise BadParameter("Die Tabelle muss im .csv Format vorliegen")

        unpaid_table_reader = csv.DictReader(unpaid_table_file).reader
        headers = next(unpaid_table_reader)
        aktionen = headers[1:-3]
        index_summe = headers.index("Summe")

        for row in unpaid_table_reader:
            name = row[0]
            summe = row[index_summe]
            unpaid_aktions = dict()
            if not name:
                continue

            for i, aktion_name in enumerate(aktionen, start=1):
                unpaid_value = row[i]
                if not unpaid_value:
                    continue
                unpaid_aktions[aktion_name] = unpaid_value

            self.unpaid.append(UnpaidRow(name, unpaid_aktions, summe))

    def render_template(self) -> str:
        out = ""

        for row in self.unpaid:
            if out:
                out += 6 * "\n"

            expanding_template = self.template
            expanding_template = self.TEMPLATE_NAME_PATTERN.sub(row.name, expanding_template)
            expanding_template = self.TEMPLATE_LIST_PATTERN.sub(row.render_aktion_list(), expanding_template)
            expanding_template = self.TEMPLATE_SUMME_PATTERN.sub(row.summe, expanding_template)

            out += expanding_template

        return out


@click.group()
def reminder_group():
    """Mahnungstexte aus dem unpaid Command erstellen"""
    pass


@reminder_group.command()
@click.argument('path',
                metavar='PATH',
                nargs=1,
                type=click.Path(exists=True, file_okay=False, resolve_path=True),
                default=Path.cwd())
def init(path: str):
    """
    Erstellt eine Tabelle für nicht bezahlte Aktionen

    \b
    PATH \tDer Speicherort für die Calc Datei
    """
    calc_file = os.path.join(src.__root_dir__, "resources", "00 Ausstehende Beiträge.ods")

    try:
        new_calc_file_location = shutil.copy(calc_file, path)
        click.echo(f'Die Tabelle wurde erfolgreich gespeichert. {new_calc_file_location}')
    except FileNotFoundError:
        click.echo(f'Die Tabelle konnte nicht im resources-Ordner gefunden werden.')
    except Exception as e:
        click.echo(f'Fehler beim Kopieren der Datei: {e}')


@reminder_group.command()
@click.option('-t', '--template', 'template_file',
              type=click.File(),
              help="Pfad der Template Datei im .txt Format")
@click.option('-l', '--list', 'unpaid_table_file',
              type=click.File(),
              help="Pfad für die Mahnungs Tabelle im .csv Format")
@click.option('-o', '--out', 'out',
              type=click.File(mode='w'),
              default=os.getcwd() + "/out.txt",
              help="Pfad für die Output Datei. DEFAULT: ./out.txt")
def template(template_file: TextIOWrapper,
             unpaid_table_file: TextIOWrapper,
             out: TextIOWrapper):
    """
    Expandiert einen Mahnungstext mit den Infos aus der Mahnungs Calc Datei.

    Dafür wird eine CSV Version der Ausstehende Beiträge Tabelle, sowie eine Template TXT Datei benötigt.

    Die Template-Datei wird mit den entsprechenden Informationen expandiert.
    Folgende Platzhalten stehen dabei zur Verfügung:

    \b
    {name} :\t Name der Person
    {list} :\t Liste der Unbezahlten Aktionen
    {summe} :\t Die gesamte Summe der Aktionen
    """
    template_renderer = TemplateRenderer(template_file, unpaid_table_file)
    rendered_text = template_renderer.render_template()

    out.write(rendered_text)
    click.echo(f"Das Template wurde erfolgreich gerendert und in {out.name} gespeichert.")
