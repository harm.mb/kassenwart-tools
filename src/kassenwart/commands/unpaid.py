import re
from dataclasses import dataclass, field
from operator import attrgetter

from src.kassenwart import utils
from src.kassenwart.commands.extract.banking import Transaction
from src.kassenwart.commands.extract.participant import Participant


@dataclass(frozen=True)
class UnpaidResult:
    paid: \
        list[tuple[Participant, Transaction]] = field(init=False, default_factory=list)
    unclear: \
        list[tuple[Participant, list[Transaction]]] = field(init=False, default_factory=list)
    unpaid: \
        list[Participant] = field(init=False, default_factory=list)
    matchless_transactions: \
        list[Transaction] = field(init=False, default_factory=list)

    def sort(self) -> None:
        self.paid.sort(key=lambda x: (x[0].firstname, x[0].lastname))
        self.unclear.sort(key=lambda x: (x[0].lastname, x[0].firstname))
        self.unpaid.sort(key=attrgetter('firstname', 'lastname'))
        self.matchless_transactions.sort(key=attrgetter('date'))

    def get_filtered(self, search_name):
        filtered = UnpaidResult()
        filtered.paid.extend(
            filter(lambda x: x[0].match_name(search_name), self.paid))
        filtered.unclear.extend(
            filter(lambda x: x[0].match_name(search_name), self.unclear))
        filtered.unpaid.extend(
            filter(lambda x: x.match_name(search_name), self.unpaid))
        return filtered


def transaction_match(transaction: Transaction,
                      participant: Participant) -> bool:
    """"""
    # TODO: Dokumentation!!
    re_firstname = fr'\b{utils.replace_umlaute(participant.firstname)}\b'
    re_lastname = fr'\b{utils.replace_umlaute(participant.lastname)}\b'
    info_str = transaction.purpose + " " + transaction.owner

    match = 0
    goal = 2

    if re.search(re_firstname, transaction.purpose, re.IGNORECASE):
        match += 1
    if re.search(re_lastname, info_str, re.IGNORECASE):
        match += 1

    return match >= goal


def resolve_unpaid(transactions: list[Transaction],
                   participants: list[Participant],
                   search_name: str = "") -> UnpaidResult:
    """"""
    # TODO: Dokumentation!!
    out: UnpaidResult = UnpaidResult()

    for participant in participants:
        matched_transactions: list[Transaction] = []
        for transaction in transactions:
            if transaction_match(transaction, participant):
                matched_transactions.append(transaction)

        if len(matched_transactions) == 1:
            out.paid.append((participant, matched_transactions[0]))
        elif len(matched_transactions) > 1:
            out.unclear.append((participant, matched_transactions))
        else:
            out.unpaid.append(participant)

    for transaction in transactions:
        in_paid = any(transaction == x for _, x in out.paid)
        in_unclear = any(transaction in x for _, x in out.unclear)

        if not in_paid and not in_unclear:
            out.matchless_transactions.append(transaction)

    out.sort()
    if search_name:
        return out.get_filtered(search_name)
    return out
