import csv
import datetime
import decimal
from dataclasses import dataclass
from datetime import date

import mt940


@dataclass
class Transaction:
    amount: decimal.Decimal
    purpose: str
    owner: str
    date: date

    def __str__(self):
        return f'{self.amount}€ {self.date.strftime("%d.%m.%y")} {self.purpose}'


class BankingExtraction:
    files: list[str] = []

    def __init__(self, file: str | list[str]):
        """
        Speichert den oder die Datei Pfade in einer Liste
        """
        if type(file) is list or type(file) is tuple:
            for f in file:
                self.files.append(f)
        else:
            self.files.append(file)

    @staticmethod
    def __parse_mt940_transactions(mt940_file: str) -> list[Transaction]:
        """Extrahiert alle Transaktionen aus einer MT-940 Datei"""
        transactions: list[Transaction] = []
        raw_transactions = mt940.parse(mt940_file)

        for transaction in raw_transactions:
            data = transaction.data
            entry = Transaction(
                amount=data.get('amount').amount,
                purpose=str(data.get('purpose') or ''),
                owner=str(data.get('applicant_name') or ''),
                date=data.get('date')
            )

            if entry.amount < 0:
                continue

            transactions.append(entry)
        return transactions

    @staticmethod
    def __parse_gnu_cash_transactions(reader: csv.DictReader) -> list[Transaction]:
        transactions: list[Transaction] = []
        for row in reader:
            if row['Abgleichen'] == 'n':
                continue

            transactions.append(
                Transaction(
                    date=datetime.datetime.strptime(row['Datum'], '%d.%m.%Y'),
                    purpose=f"{row['Beschreibung']}\t{row['Bemerkungen']}",
                    amount=decimal.Decimal(
                        value=row['Wert numerisch.'].replace(',', '.')),
                    owner=""
                )
            )
        return transactions

    @staticmethod
    def __parse_intern_transactions(reader: csv.DictReader) -> list[Transaction]:
        transactions: list[Transaction] = []
        for row in reader:
            transactions.append(
                Transaction(
                    row['amount'],
                    row['purpose'],
                    row['owner'],
                    row['date']
                ))
        return transactions

    @staticmethod
    def __parse_csv_transactions(csv_file: str) -> list[Transaction]:
        """Extrahiert alle Transaktionen aus einer CSV Datei aus GnuCash"""
        transactions: list[Transaction] = []

        with open(csv_file, newline='') as file:
            reader = csv.DictReader(file)
            if "BuchungsID" in reader.fieldnames:
                transactions.extend(
                    BankingExtraction.__parse_gnu_cash_transactions(reader))
            elif "amount" in reader.fieldnames:
                transactions.extend(
                    BankingExtraction.__parse_intern_transactions(reader))
        return transactions

    def get_transactions(self) -> list[Transaction]:
        """Liefert alle Transaktionen zurück"""
        transactions: list[Transaction] = []
        for file in self.files:
            if file.lower().endswith('.mta'):
                # MT-940
                transactions.extend(
                    BankingExtraction.__parse_mt940_transactions(file)
                )
            elif file.lower().endswith('.csv'):
                # CSV
                transactions.extend(
                    BankingExtraction.__parse_csv_transactions(file)
                )
            else:
                raise RuntimeError("Unsupported Filetype! Only .mta & .csv")
        return transactions
