import csv
import re
from dataclasses import dataclass

from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdftypes import resolve1
from pdfminer.psparser import PSLiteral, PSKeyword
from pdfminer.utils import decode_text


@dataclass
class Participant:
    firstname: str = ""
    lastname: str = ""
    age: int = ""
    residence: str = ""
    street: str = ""
    # TODO: Unnötige Teilnehmenden Informationen entfernen

    def fullname(self):
        return f'{self.firstname} {self.lastname}'

    def match_name(self, name) -> bool:
        re_fullname = r'.*'.join(fr'\b{word}\b' for word in name.split())
        return bool(re.search(re_fullname, self.fullname(), re.IGNORECASE))


class ParticipantExtraction:
    def __init__(self, file_list: str | list[str]):
        """
        Speichert den oder die Datei Pfade in einer Liste
        """
        self.files: list[str] = []
        if type(file_list) is list or type(file_list) is tuple:
            for f in file_list:
                self.files.append(f)
        else:
            self.files.append(file_list)

    @staticmethod
    def _decode_value(value) -> str:  # Todo: Typ des Parameter angeben
        """Dekodiert die Werte die aus der PDF-Datei kommen"""
        # decode PSLiteral, PSKeyword
        if isinstance(value, (PSLiteral, PSKeyword)):
            value = value.name
        # decode bytes
        if isinstance(value, bytes):
            value = decode_text(value)
        return value

    @staticmethod
    def _extract_pdf_participants(pdf_file: str) -> list[Participant]:
        """Extrahiert alle Teilnehmer:innen aus einer PDF-Datei"""
        fields: list[tuple[str, str]] = []
        with open(pdf_file, 'rb') as file:
            parser = PDFParser(file)
            doc = PDFDocument(parser)
            res = resolve1(doc.catalog)

            if 'AcroForm' not in res:
                raise ValueError("No AcroForm Found")

            form_fields = resolve1(doc.catalog['AcroForm'])['Fields']

            for f in form_fields:
                field = resolve1(f)
                name, values = field.get('T'), field.get('V')
                name = decode_text(name)
                values = resolve1(values)

                if isinstance(values, list):
                    values = [ParticipantExtraction._decode_value(v) for v in values]
                else:
                    values = ParticipantExtraction._decode_value(values)

                fields.append((name, values))

        if len(field) == 0: raise RuntimeError("Could not extract fields from PDF")

        participants: list[Participant] = []

        firstname_reg = re.compile(r'^(Vorname)')
        lastname_reg = re.compile(r'^(Name)')
        age_reg = re.compile(r'^(Alter)')
        residence_reg = re.compile(r'^(Wohnort)')
        street_reg = re.compile(r'^(Straße)')

        counter = 0
        match = False
        participant = Participant()
        for (name, value) in fields:
            if firstname_reg.search(name):
                counter += 1
                if value not in (None, ""):
                    participant.firstname = value
                    match = True

            if lastname_reg.search(name):
                counter += 1
                if value not in (None, ""):
                    participant.lastname = value
                    match = True

            if age_reg.search(name):
                counter += 1
                if value not in (None, ""):
                    participant.age = value
                    match = True

            if residence_reg.search(name):
                counter += 1
                if value not in (None, ""):
                    participant.residence = value
                    match = True

            if street_reg.search(name):
                counter += 1
                if value not in (None, ""):
                    participant.street = value
                    match = True

            # Es werden alle 5 Felder abgewartet, sodass alle Werte erfasst werden können
            if match and counter % 5 == 0:
                participants.append(participant)
                participant = Participant()
                match = False

        return participants

    @staticmethod
    def _extract_csv_participants(csv_file: str) -> list[Participant]:
        """Extrahiert alle Teilnehmer:innen aus einer CSV Datei"""
        participants: list[Participant] = []

        with open(csv_file, newline='') as file:
            reader = csv.DictReader(file)
            for row in reader:
                participants.append(Participant(
                    firstname=row['firstname'],
                    lastname=row['lastname'],
                    age=int(row['age']),
                    residence=row['residence'],
                    street=row['street']
                ))

        return participants

    @staticmethod
    def _extract_txt_participants(txt_file: str) -> list[Participant]:
        """Extrahiert alle Teilnehmer:innen aus einer Textdatei"""
        participants: list[Participant] = []

        with open(txt_file) as file:
            for row in file.readlines():
                row = row.strip()
                if row and row.__contains__(' '):
                    firstname, lastname = row.split(maxsplit=1)
                    participants.append(Participant(firstname, lastname))
                else:
                    print("WARN: Row is empty or does not contain name:", row)

        return participants

    def get_participants(self) -> list[Participant]:
        """
        Extrahiert alle Teilnehmenden von der Liste und gibt sie als `Participant` zurück
        """
        participants: list[Participant] = []

        for file in self.files:
            if file.lower().endswith('.pdf'):
                # PDF
                participants.extend(self._extract_pdf_participants(file))
            elif file.lower().endswith('.csv'):
                # CSV
                participants.extend(self._extract_csv_participants(file))
            elif file.lower().endswith('.txt'):
                # Text
                participants.extend(self._extract_txt_participants(file))
            else:
                raise RuntimeError("Unsupported Filetype! Only CSV, PDF")

        return participants
