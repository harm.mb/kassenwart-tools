from pathlib import Path

__version__ = '0.0.8'

__root_dir__ = Path(__file__).parent.parent
