# Kassenwart Tools

Ein kleines CLI Tool, was die Arbeit eines:einer Kassenwart:in erleichtert.

## Installation

### Brachiale Installation

Das Projekt kann an eine Stelle deiner Wahl abgelegt werden, um eine integration in euer
Betriebssystem könnt ihr euch selber kümmern.

Erstellt zunächst ein Virtual Environment für die Python Umgebung. Falls du einen anderen Pfad haben möchtest,
musst du den in `kssnwrt_cli.sh` anpassen.

> `python3 -m venv venv`

Danach müsst ihr die verwendeten Python Libraries installieren.

> `pip install -r requirements.txt`

Um die Installation zu testen, kann die Versionsnummer ausgegeben werden.

> `./kssnwrt_cli.sh --version`

### Installation mit pip

Es gibt allerdings auch die Möglichkeit, dass Tool über pip zu installieren.

> `pip install .`

Wenn das Programm mit pip installiert wurde, gibt es die Möglichkeit **Auto Completion**
einzuschalten. Dafür muss folgende Zeile beim start der Shell ausgeführt werden. Am besten
in dem *rc Script*.

Für Bash ist das Beispielsweise: `~/.bashrc`

> `eval "$(_KASSENWART_CLI_COMPLETE=bash_source kassenwart_cli)"`

Wenn die Vervollständigung auch in anderen Shells funktionieren soll, muss natürlich
das `bash_source` durch einen vergleichbaren Befehl ersetzt werden.

## Dokumentation

Eigentlich ist alles im Script dokumentiert. Nutzt halt die `--help` Option
